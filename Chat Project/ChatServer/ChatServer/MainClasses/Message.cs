﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer.MainClasses
{
    public class Message
    {
        string userName;
        string mes;

        public Message() { }

        public Message(string userName, string mes)
        {
            this.userName = userName;
            this.mes = mes;
        }     
    }
}
