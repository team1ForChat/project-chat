﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer.MainClasses
{
    public class Room
    {
        public string name;
        static int num;
        public int id;
        public List<User> userList;
        public List<Message> messageList;
        public static int Num
        {
            get
            {
                return num;
            }

            set
            {
                num = value;
            }
        }
        public Room()
        {

        }
        public Room(string name)
        {
            this.name = name;
            id = Num;
            Num++;
            userList = new List<User>();
            messageList = new List<Message>();
            messageList.Add(new Message("Vasya", " Hello"));
            messageList.Add(new Message("Senya", " Hi"));
            messageList.Add(new Message("Vasya", " nice"));
        }

    }
}
