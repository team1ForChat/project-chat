﻿using ChatServer.DB;
using ChatServer.MainClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer.Server
{
   public class Response
    {
        public string data;
        IUserDAO dao = new MockDAO();
        public Response(Request req, ServerObject server)
        {
            switch (req.cmd)
            {
                case "LOGIN":
                    data = req.data + " joined the chat";
                    break;
                case "EXIT":
                    data = "Client " + req.data + " leave the chat";
                    break;
                case "MESSAGE":
                    data = req.data;
                    break;
                case "READUSERS":
                    List<User> v = dao.Read();

                    for (int i = 0; i < v.Count; i++)
                    {
                        data += string.Format("{0},{1},{2};", v[i].login, v[i].password, v[i].eMail);

                    }                    
                    break;
                case "CREATEUSER":
                    dao.Create(DecodeMessage(req));
                    break;
                default:
                    break;
            }
            lock (server.qe)
            {
                Console.Write(data);
                server.qe.Enqueue(data);
            }
        }
        public static User DecodeMessage(Request request)
        {   
            string[] stringP = request.data.Split(',');
            User u = new User(stringP[0], stringP[1], stringP[2]);        
            return u;
        }
    }
}
