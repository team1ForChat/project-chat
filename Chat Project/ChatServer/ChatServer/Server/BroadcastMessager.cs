﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatServer
{
    public class BroadcastMessager
    {
        static ServerObject serv;

        public BroadcastMessager(ServerObject server)
        {
            Thread broadcastThread = new Thread(new ThreadStart(Broadcast));
            broadcastThread.Start();
            serv = server;
        }
        public void Broadcast()
        {
            while (true)
            {
                Thread.Sleep(11);
                if (serv.qe.Count != 0)
                {                    
                    lock (serv.qe)
                    {
                        byte[] data = Encoding.Unicode.GetBytes(serv.qe.Dequeue());
                        for (int i = 0; i < serv.clients.Count; i++)
                        {
                            serv.clients[i].Stream.Write(data, 0, data.Length);
                        }
                    }

                }
            }
        }
    }
}
