﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatServer
{
    public class Request
    {
        public string cmd;
        public string data;

        public Request(string st)
        {
            if (st.Contains(":"))
            {
                string[] p = st.Split(':');
                cmd = p[0];
                data = p[1];
            }
        }
    }
}
