﻿using ChatServer.MainClasses;
using System.Collections.Generic;

namespace ChatServer.DB
{
    internal interface IUserDAO
    {
        void Create(User u);
        List<User> Read();
        void Update(User u);
        void Delete(User u);
    }
}