﻿using ChatServer.MainClasses;
using System.Collections.Generic;

namespace ChatServer.DB
{
    public interface IRoomDAO
    {
        void Create(Room r);
        List<Room> Read();
        void Update(Room r);
        void Delete(Room r);
    }
}