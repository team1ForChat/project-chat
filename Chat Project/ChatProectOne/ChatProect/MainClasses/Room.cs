﻿using System.Collections.Generic;

namespace ChatProect
{
    public class Room
    {
        public string name;
        static int num;
        public int id;
        public List<User> userList;
        public List<Message> messageList;
       

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public static int Num
        {
            get
            {
                return num;
            }

            set
            {
                num = value;
            }
        }

        //public int Id
        //{
        //    get
        //    {
        //        return id;
        //    }

        //    set
        //    {
        //        id = value;
        //    }
        //}

        public Room()
        {

        }
        public Room(string name)
        {
            this.name = name;
            id = Num;
            Num++;
            userList = new List<User>();
            messageList = new List<Message>();
            messageList.Add(new Message("Vasya"," Hello"));
            messageList.Add(new Message("Senya", " Hi"));
            messageList.Add(new Message("Vasya", " nice"));         
        }

    }
}
