﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatProect
{
    public class Message
    {
        string userName;
        string mes;
        public Message() { }
        public Message(string userName,string mes)
        {
            this.userName = userName;
            this.mes = mes;
        }
        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }
        public string Mes
        {
            get
            {
                return mes;
            }

            set
            {
                mes = value;
            }
        }

     
    }
}
