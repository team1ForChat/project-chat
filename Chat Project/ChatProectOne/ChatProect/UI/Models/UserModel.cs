﻿using ChatProect.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatProect
{
 public class UserModel
    {

         IUserDAO data = new DB.MockDAO();
        //IUserDAO data = new NetClientDAO();
        public void Create(User u)
        {
            data.Create(u);
        }
        public List<User> Read()
        {
            return data.Read();
        }
        public void Update(User u)
        {
            data.Update(u);
        }
        public void Delete(User u)
        {
            data.Delete(u);
        }
    }
}
