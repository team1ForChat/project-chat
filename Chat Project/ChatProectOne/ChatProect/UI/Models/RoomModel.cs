﻿using System.Collections.Generic;

namespace ChatProect
{
    public class RoomModel
    {
        IRoomDAO data = new DB.MockDAO();
        // IRoomDAO data = new DB.NetClientDAO();
        public void Create(Room r)
        {
            data.Create(r);
        }
        public List<Room> Read()
        {
            return data.Read();
        }
        public void Update(Room r)
        {
            data.Update(r);
        }
        public void Delete(Room r)
        {
            data.Delete(r);
        }
    }
}
