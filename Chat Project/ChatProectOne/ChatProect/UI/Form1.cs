﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ChatProect.UI.Panels.PanelProfile;
using ChatProect.UI.Panels.MainPanel;
using System.Windows.Forms;

namespace ChatProect
{
    public partial class Form1 : Form
    {
        public RoomModel roomModel = new RoomModel();
        public UserModel userModel = new UserModel();
        public List<Room> roomList;
        public List<User> users;
        public List<User> allUsers;
        int index;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            panelProfile.Controls.Clear();
            panelProfile.Controls.Add(new LogInPanel(this));
        }

        private void tabRoomUsers_Click(object sender, EventArgs e)
        {

        }
        private void GridViewMyRooms_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int n = 0;
            index = e.RowIndex;
            roomList = roomModel.Read();
            foreach (var Room in roomList)
            {
                if (n == index)
                {
                    GridViewRoomUsers.DataSource = Room.userList;
                }
                n++;
            }

            //BindingSource source1 = new BindingSource();
            //source1.DataSource = GridViewMyRooms;
            //GridViewRoomUsers.DataSource = source1;
        }

        private void JoinRoom_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Clear();
            TextBox SendBox = new TextBox();
            SendBox.Location = new Point(301, 432);
            SendBox.Size = new Size(429, 20);
            this.Controls.Add(SendBox);

            Button send = new Button();
            send.Size = new Size(64, 23);
            send.Location = new Point(736, 430);
            send.Text = "Send";
            this.Controls.Add(send);

            DataGridView messageDataGridView = new DataGridView();
            messageDataGridView.Size = mainPanel.Size;
            messageDataGridView.ColumnHeadersVisible = false;
            messageDataGridView.RowHeadersVisible = false;
            //  messageDataGridView.Columns[0].AutoSizeMode=AutoSizeMode.; 
            messageDataGridView.CellBorderStyle = DataGridViewCellBorderStyle.None;
            messageDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            foreach (var Room in roomList)
            {
                if (Room.id == index)
                {
                    messageDataGridView.DataSource = Room.messageList;
                }
            }
            mainPanel.Controls.Add(messageDataGridView);
        }

        private void CreateRoom_Click(object sender, EventArgs e)
        {
            string roomName = EnterRoomName.Text;
            roomModel.Create(new Room(roomName));

            roomList = roomModel.Read();
            GridViewAllRooms.DataSource = roomList;

        }

        private void DeleteRoomButton_Click(object sender, EventArgs e)
        {
            int n = 0;
            foreach (var Room in roomModel.Read())
            {
                if (n == index)
                {
                    roomModel.Delete(Room);
                    GridViewAllRooms.DataSource = roomModel.Read();                   
                }
                n++;
            }
        }

        private void BanButton_Click(object sender, EventArgs e)
        {

        }

        private void UserInfo_Click(object sender, EventArgs e)
        {
            mainPanel.Controls.Add(new UI.Panels.MainPanel.RegistrationPanel(this));
        }
    }
}
