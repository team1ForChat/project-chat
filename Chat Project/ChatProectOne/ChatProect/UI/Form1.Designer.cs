﻿namespace ChatProect
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.tabControlRooms = new System.Windows.Forms.TabControl();
            this.tabAllRooms = new System.Windows.Forms.TabPage();
            this.GridViewAllRooms = new System.Windows.Forms.DataGridView();
            this.tabMyRooms = new System.Windows.Forms.TabPage();
            this.GridViewRooms = new System.Windows.Forms.DataGridView();
            this.tabControlUsers = new System.Windows.Forms.TabControl();
            this.tabRoomUsers = new System.Windows.Forms.TabPage();
            this.GridViewRoomUsers = new System.Windows.Forms.DataGridView();
            this.tabAllUsers = new System.Windows.Forms.TabPage();
            this.GridViewAllUsers = new System.Windows.Forms.DataGridView();
            this.panelProfile = new System.Windows.Forms.Panel();
            this.CreateRoom = new System.Windows.Forms.Button();
            this.EnterRoomName = new System.Windows.Forms.TextBox();
            this.UserInfo = new System.Windows.Forms.Button();
            this.JoinRoom = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.BanButton = new System.Windows.Forms.Button();
            this.DeleteRoomButton = new System.Windows.Forms.Button();
            this.tabControlRooms.SuspendLayout();
            this.tabAllRooms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAllRooms)).BeginInit();
            this.tabMyRooms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewRooms)).BeginInit();
            this.tabControlUsers.SuspendLayout();
            this.tabRoomUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewRoomUsers)).BeginInit();
            this.tabAllUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAllUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlRooms
            // 
            this.tabControlRooms.Controls.Add(this.tabAllRooms);
            this.tabControlRooms.Controls.Add(this.tabMyRooms);
            this.tabControlRooms.Location = new System.Drawing.Point(2, 2);
            this.tabControlRooms.Name = "tabControlRooms";
            this.tabControlRooms.SelectedIndex = 0;
            this.tabControlRooms.Size = new System.Drawing.Size(297, 181);
            this.tabControlRooms.TabIndex = 0;
            // 
            // tabAllRooms
            // 
            this.tabAllRooms.Controls.Add(this.GridViewAllRooms);
            this.tabAllRooms.Location = new System.Drawing.Point(4, 22);
            this.tabAllRooms.Name = "tabAllRooms";
            this.tabAllRooms.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllRooms.Size = new System.Drawing.Size(289, 155);
            this.tabAllRooms.TabIndex = 0;
            this.tabAllRooms.Text = "All rooms";
            this.tabAllRooms.UseVisualStyleBackColor = true;
            // 
            // GridViewAllRooms
            // 
            this.GridViewAllRooms.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridViewAllRooms.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.GridViewAllRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewAllRooms.ColumnHeadersVisible = false;
            this.GridViewAllRooms.Location = new System.Drawing.Point(0, 0);
            this.GridViewAllRooms.Name = "GridViewAllRooms";
            this.GridViewAllRooms.RowHeadersVisible = false;
            this.GridViewAllRooms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridViewAllRooms.Size = new System.Drawing.Size(289, 155);
            this.GridViewAllRooms.TabIndex = 0;
            this.GridViewAllRooms.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridViewMyRooms_CellMouseClick);
            // 
            // tabMyRooms
            // 
            this.tabMyRooms.Controls.Add(this.GridViewRooms);
            this.tabMyRooms.Location = new System.Drawing.Point(4, 22);
            this.tabMyRooms.Name = "tabMyRooms";
            this.tabMyRooms.Padding = new System.Windows.Forms.Padding(3);
            this.tabMyRooms.Size = new System.Drawing.Size(289, 155);
            this.tabMyRooms.TabIndex = 1;
            this.tabMyRooms.Text = "My Rooms";
            this.tabMyRooms.UseVisualStyleBackColor = true;
            // 
            // GridViewRooms
            // 
            this.GridViewRooms.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewRooms.Location = new System.Drawing.Point(0, 0);
            this.GridViewRooms.Name = "GridViewRooms";
            this.GridViewRooms.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridViewRooms.Size = new System.Drawing.Size(289, 159);
            this.GridViewRooms.TabIndex = 0;
            // 
            // tabControlUsers
            // 
            this.tabControlUsers.Controls.Add(this.tabRoomUsers);
            this.tabControlUsers.Controls.Add(this.tabAllUsers);
            this.tabControlUsers.Location = new System.Drawing.Point(6, 238);
            this.tabControlUsers.Name = "tabControlUsers";
            this.tabControlUsers.SelectedIndex = 0;
            this.tabControlUsers.Size = new System.Drawing.Size(293, 188);
            this.tabControlUsers.TabIndex = 1;
            // 
            // tabRoomUsers
            // 
            this.tabRoomUsers.Controls.Add(this.GridViewRoomUsers);
            this.tabRoomUsers.Location = new System.Drawing.Point(4, 22);
            this.tabRoomUsers.Name = "tabRoomUsers";
            this.tabRoomUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabRoomUsers.Size = new System.Drawing.Size(285, 162);
            this.tabRoomUsers.TabIndex = 0;
            this.tabRoomUsers.Text = "Room Users";
            this.tabRoomUsers.UseVisualStyleBackColor = true;
            this.tabRoomUsers.Click += new System.EventHandler(this.tabRoomUsers_Click);
            // 
            // GridViewRoomUsers
            // 
            this.GridViewRoomUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridViewRoomUsers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.GridViewRoomUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewRoomUsers.ColumnHeadersVisible = false;
            this.GridViewRoomUsers.Location = new System.Drawing.Point(0, 0);
            this.GridViewRoomUsers.Name = "GridViewRoomUsers";
            this.GridViewRoomUsers.RowHeadersVisible = false;
            this.GridViewRoomUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridViewRoomUsers.Size = new System.Drawing.Size(285, 162);
            this.GridViewRoomUsers.TabIndex = 0;
            // 
            // tabAllUsers
            // 
            this.tabAllUsers.Controls.Add(this.GridViewAllUsers);
            this.tabAllUsers.Location = new System.Drawing.Point(4, 22);
            this.tabAllUsers.Name = "tabAllUsers";
            this.tabAllUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabAllUsers.Size = new System.Drawing.Size(285, 162);
            this.tabAllUsers.TabIndex = 1;
            this.tabAllUsers.Text = "All Users";
            this.tabAllUsers.UseVisualStyleBackColor = true;
            // 
            // GridViewAllUsers
            // 
            this.GridViewAllUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GridViewAllUsers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.GridViewAllUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridViewAllUsers.ColumnHeadersVisible = false;
            this.GridViewAllUsers.Location = new System.Drawing.Point(0, 0);
            this.GridViewAllUsers.Name = "GridViewAllUsers";
            this.GridViewAllUsers.RowHeadersVisible = false;
            this.GridViewAllUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.GridViewAllUsers.Size = new System.Drawing.Size(285, 162);
            this.GridViewAllUsers.TabIndex = 0;
            // 
            // panelProfile
            // 
            this.panelProfile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelProfile.Location = new System.Drawing.Point(301, 2);
            this.panelProfile.Name = "panelProfile";
            this.panelProfile.Size = new System.Drawing.Size(499, 106);
            this.panelProfile.TabIndex = 2;
            // 
            // CreateRoom
            // 
            this.CreateRoom.Location = new System.Drawing.Point(6, 208);
            this.CreateRoom.Name = "CreateRoom";
            this.CreateRoom.Size = new System.Drawing.Size(92, 20);
            this.CreateRoom.TabIndex = 4;
            this.CreateRoom.Text = "Create room";
            this.CreateRoom.UseVisualStyleBackColor = true;
            this.CreateRoom.Click += new System.EventHandler(this.CreateRoom_Click);
            // 
            // EnterRoomName
            // 
            this.EnterRoomName.Location = new System.Drawing.Point(134, 208);
            this.EnterRoomName.Name = "EnterRoomName";
            this.EnterRoomName.Size = new System.Drawing.Size(136, 20);
            this.EnterRoomName.TabIndex = 5;
            // 
            // UserInfo
            // 
            this.UserInfo.Location = new System.Drawing.Point(9, 432);
            this.UserInfo.Name = "UserInfo";
            this.UserInfo.Size = new System.Drawing.Size(110, 24);
            this.UserInfo.TabIndex = 6;
            this.UserInfo.Text = "Show users info";
            this.UserInfo.UseVisualStyleBackColor = true;
            this.UserInfo.Click += new System.EventHandler(this.UserInfo_Click);
            // 
            // JoinRoom
            // 
            this.JoinRoom.Location = new System.Drawing.Point(6, 185);
            this.JoinRoom.Name = "JoinRoom";
            this.JoinRoom.Size = new System.Drawing.Size(92, 23);
            this.JoinRoom.TabIndex = 7;
            this.JoinRoom.Text = "Join room";
            this.JoinRoom.UseVisualStyleBackColor = true;
            this.JoinRoom.Click += new System.EventHandler(this.JoinRoom_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.mainPanel.Location = new System.Drawing.Point(301, 114);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(499, 342);
            this.mainPanel.TabIndex = 10;
            // 
            // BanButton
            // 
            this.BanButton.Location = new System.Drawing.Point(134, 432);
            this.BanButton.Name = "BanButton";
            this.BanButton.Size = new System.Drawing.Size(75, 23);
            this.BanButton.TabIndex = 11;
            this.BanButton.Text = "Ban";
            this.BanButton.UseVisualStyleBackColor = true;
            this.BanButton.Visible = false;
            this.BanButton.Click += new System.EventHandler(this.BanButton_Click);
            // 
            // DeleteRoomButton
            // 
            this.DeleteRoomButton.Location = new System.Drawing.Point(134, 185);
            this.DeleteRoomButton.Name = "DeleteRoomButton";
            this.DeleteRoomButton.Size = new System.Drawing.Size(136, 23);
            this.DeleteRoomButton.TabIndex = 12;
            this.DeleteRoomButton.Text = "Delete Room";
            this.DeleteRoomButton.UseVisualStyleBackColor = true;
            this.DeleteRoomButton.Visible = false;
            this.DeleteRoomButton.Click += new System.EventHandler(this.DeleteRoomButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 462);
            this.Controls.Add(this.DeleteRoomButton);
            this.Controls.Add(this.BanButton);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.JoinRoom);
            this.Controls.Add(this.UserInfo);
            this.Controls.Add(this.EnterRoomName);
            this.Controls.Add(this.CreateRoom);
            this.Controls.Add(this.panelProfile);
            this.Controls.Add(this.tabControlUsers);
            this.Controls.Add(this.tabControlRooms);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControlRooms.ResumeLayout(false);
            this.tabAllRooms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAllRooms)).EndInit();
            this.tabMyRooms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewRooms)).EndInit();
            this.tabControlUsers.ResumeLayout(false);
            this.tabRoomUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewRoomUsers)).EndInit();
            this.tabAllUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridViewAllUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TabControl tabControlRooms;
        public System.Windows.Forms.TabPage tabAllRooms;
        public System.Windows.Forms.DataGridView GridViewAllRooms;
        public System.Windows.Forms.TabPage tabMyRooms;
        public System.Windows.Forms.TabControl tabControlUsers;
        public System.Windows.Forms.TabPage tabRoomUsers;
        public System.Windows.Forms.DataGridView GridViewRoomUsers;
        public System.Windows.Forms.TabPage tabAllUsers;
        public System.Windows.Forms.Panel panelProfile;
        public System.Windows.Forms.DataGridView GridViewRooms;
        public System.Windows.Forms.DataGridView GridViewAllUsers;
        public System.Windows.Forms.Button CreateRoom;
        public System.Windows.Forms.TextBox EnterRoomName;
        public System.Windows.Forms.Button UserInfo;
        public System.Windows.Forms.Button JoinRoom;
        public System.Windows.Forms.Panel mainPanel;
        public System.Windows.Forms.Button BanButton;
        public System.Windows.Forms.Button DeleteRoomButton;
    }
}

