﻿using System.Drawing;
using System.Windows.Forms;

namespace ChatProect.UI.Panels.MainPanel
{
    public class RegistrationPanel:Panel
    {
        private Form1 form;
        private Label RegistrationLabel;
        private Label UserNameLabel;
        private Label PasswordLabel;
        private Label ConfirmationLabel;
        private Button RegisterButton;
        private Button CancelRegisterButton;
        private TextBox UserNameTexBox;
        private TextBox PasswordTextBox;
        private Label EmailLabel;
        private TextBox EmailTextBox;
        private TextBox ConfirmationTexBox;

        public RegistrationPanel(Form1 form)
        {
            this.form = form;
            Size = form.mainPanel.Size;
            InitializeComponent();                 
        }

   
        public void CreatePanel()
        {
           
        }

        private void InitializeComponent()
        {
            RegistrationLabel = new System.Windows.Forms.Label();
            UserNameLabel = new System.Windows.Forms.Label();
            PasswordLabel = new System.Windows.Forms.Label();
            ConfirmationLabel = new System.Windows.Forms.Label();
            RegisterButton = new System.Windows.Forms.Button();
            CancelRegisterButton = new System.Windows.Forms.Button();
            UserNameTexBox = new System.Windows.Forms.TextBox();
            PasswordTextBox = new System.Windows.Forms.TextBox();
            ConfirmationTexBox = new System.Windows.Forms.TextBox();
            EmailLabel = new System.Windows.Forms.Label();
            EmailTextBox = new System.Windows.Forms.TextBox();
            SuspendLayout();
            // 
            // RegistrationLabel
            // 
            RegistrationLabel.AutoSize = true;
            RegistrationLabel.Location = new System.Drawing.Point(216, 39);
            RegistrationLabel.Name = "RegistrationLabel";
            RegistrationLabel.Size = new System.Drawing.Size(63, 13);
            RegistrationLabel.TabIndex = 0;
            RegistrationLabel.Text = "Registration";
            // 
            // UserNameLabel
            // 
            UserNameLabel.AutoSize = true;
            UserNameLabel.Location = new System.Drawing.Point(99, 99);
            UserNameLabel.Name = "UserNameLabel";
            UserNameLabel.Size = new System.Drawing.Size(64, 13);
            UserNameLabel.TabIndex = 0;
            UserNameLabel.Text = "User Name*";
            // 
            // PasswordLabel
            // 
            PasswordLabel.AutoSize = true;
            PasswordLabel.Location = new System.Drawing.Point(99, 184);
            PasswordLabel.Name = "PasswordLabel";
            PasswordLabel.Size = new System.Drawing.Size(57, 13);
            PasswordLabel.TabIndex = 0;
            PasswordLabel.Text = "Password*";
            // 
            // ConfirmationLabel
            // 
            ConfirmationLabel.AutoSize = true;
            ConfirmationLabel.Location = new System.Drawing.Point(99, 224);
            ConfirmationLabel.Name = "ConfirmationLabel";
            ConfirmationLabel.Size = new System.Drawing.Size(69, 13);
            ConfirmationLabel.TabIndex = 0;
            ConfirmationLabel.Text = "Confirmation*";
            // 
            // RegisterButton
            // 
            RegisterButton.Location = new System.Drawing.Point(268, 261);
            RegisterButton.Name = "RegisterButton";
            RegisterButton.Size = new System.Drawing.Size(75, 23);
            RegisterButton.TabIndex = 0;
            RegisterButton.Text = "Register";
            RegisterButton.UseVisualStyleBackColor = true;
            RegisterButton.Click += new System.EventHandler(RegisterButton_Click);
            // 
            // CancelRegisterButton
            // 
            CancelRegisterButton.Location = new System.Drawing.Point(180, 261);
            CancelRegisterButton.Name = "CancelRegisterButton";
            CancelRegisterButton.Size = new System.Drawing.Size(75, 23);
            CancelRegisterButton.TabIndex = 0;
            CancelRegisterButton.Text = "Cancel";
            CancelRegisterButton.UseVisualStyleBackColor = true;
            CancelRegisterButton.Click += new System.EventHandler(CancelRegisterButton_Click);
            // 
            // UserNameTexBox
            // 
            UserNameTexBox.Location = new System.Drawing.Point(199, 93);
            UserNameTexBox.Name = "UserNameTexBox";
            UserNameTexBox.Size = new System.Drawing.Size(138, 20);
            UserNameTexBox.TabIndex = 0;
            // 
            // PasswordTextBox
            // 
            PasswordTextBox.Location = new System.Drawing.Point(199, 184);
            PasswordTextBox.Name = "PasswordTextBox";
            PasswordTextBox.Size = new System.Drawing.Size(138, 20);
            PasswordTextBox.TabIndex = 0;
            // 
            // ConfirmationTexBox
            // 
            ConfirmationTexBox.Location = new System.Drawing.Point(199, 224);
            ConfirmationTexBox.Name = "ConfirmationTexBox";
            ConfirmationTexBox.Size = new System.Drawing.Size(138, 20);
            ConfirmationTexBox.TabIndex = 0;
            // 
            // EmailLabel
            // 
            EmailLabel.AutoSize = true;
            EmailLabel.Location = new System.Drawing.Point(99, 144);
            EmailLabel.Name = "EmailLabel";
            EmailLabel.Size = new System.Drawing.Size(36, 13);
            EmailLabel.TabIndex = 0;
            EmailLabel.Text = "Email*";
            // 
            // EmailTextBox
            // 
            EmailTextBox.Location = new System.Drawing.Point(199, 141);
            EmailTextBox.Name = "EmailTextBox";
            EmailTextBox.Size = new System.Drawing.Size(138, 20);
            EmailTextBox.TabIndex = 0;
            // 
            // RegistrationPanel
            // 
            Controls.Add(RegistrationLabel);
            Controls.Add(UserNameLabel);
            Controls.Add(PasswordLabel);
            Controls.Add(ConfirmationLabel);
            Controls.Add(EmailLabel);
            Controls.Add(RegisterButton);
            Controls.Add(UserNameTexBox);
            Controls.Add(CancelRegisterButton);
            Controls.Add(PasswordTextBox);
            Controls.Add(EmailTextBox);
            Controls.Add(ConfirmationTexBox);
            ResumeLayout(false);
            PerformLayout();

        }

        private void RegisterButton_Click(object sender, System.EventArgs e)
        {
            if (UserNameTexBox.Text == "" || PasswordTextBox.Text == "" || EmailTextBox.Text == "" || ConfirmationTexBox.Text=="")
            {
                MessageBox.Show("Обязательные поля не заполнены");
            }
            else
            {
                if (ConfirmationTexBox.Text != PasswordTextBox.Text)
                {
                    MessageBox.Show("Пароли не совпадают!");
                }
                else
                {
                    foreach (User us in form.userModel.Read())
                    {
                        if (UserNameTexBox.Text == us.login)
                        {
                            MessageBox.Show("Пользователь с таким логином уже зарегистрирован");
                            return;
                        }
                    }
                  
                    User user = new User(UserNameTexBox.Text, PasswordTextBox.Text, EmailTextBox.Text);
                    form.userModel.Create(user);
                    MessageBox.Show("Регистрация успешно завершена!");
                    form.mainPanel.Controls.Clear();
                }                
            }           
        }

        private void CancelRegisterButton_Click(object sender, System.EventArgs e)
        {
            form.mainPanel.Controls.Clear();
        }
    }
}
