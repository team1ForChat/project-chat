﻿using ChatProect.UI.Models;
using System;
using System.Drawing;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

namespace ChatProect.UI.Panels.MainPanel
{
    public class PasswordRestorePanel : Panel
    {
        Form1 form;
        private Label EmailEnterLabel;
        private TextBox EmailEnterTextBox;
        private Button RestorePassOkButton;
        private Button RestorePasscancelButton;
        private Label RestorePasswordLabel;

        public PasswordRestorePanel(Form1 form)
        {
            this.form = form;
            this.Size = form.mainPanel.Size;
            InitializeComponent();   
        }


        private void InitializeComponent()
        {
            this.RestorePasswordLabel = new System.Windows.Forms.Label();
            this.EmailEnterLabel = new System.Windows.Forms.Label();
            this.EmailEnterTextBox = new System.Windows.Forms.TextBox();
            this.RestorePassOkButton = new System.Windows.Forms.Button();
            this.RestorePasscancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RestorePasswordLabel
            // 
            this.RestorePasswordLabel.AutoSize = true;
            this.RestorePasswordLabel.Location = new System.Drawing.Point(212, 18);
            this.RestorePasswordLabel.Name = "RestorePasswordLabel";
            this.RestorePasswordLabel.Size = new System.Drawing.Size(90, 13);
            this.RestorePasswordLabel.TabIndex = 0;
            this.RestorePasswordLabel.Text = "RestorePassword";
            // 
            // EmailEnterLabel
            // 
            this.EmailEnterLabel.AutoSize = true;
            this.EmailEnterLabel.Location = new System.Drawing.Point(65, 72);
            this.EmailEnterLabel.Name = "EmailEnterLabel";
            this.EmailEnterLabel.Size = new System.Drawing.Size(85, 13);
            this.EmailEnterLabel.TabIndex = 0;
            this.EmailEnterLabel.Text = "Enter Your Email";
            // 
            // EmailEnterTextBox
            // 
            this.EmailEnterTextBox.Location = new System.Drawing.Point(155, 72);
            this.EmailEnterTextBox.Name = "EmailEnterTextBox";
            this.EmailEnterTextBox.Size = new System.Drawing.Size(250, 20);
            this.EmailEnterTextBox.TabIndex = 0;
            // 
            // RestorePassOkButton
            // 
            this.RestorePassOkButton.Location = new System.Drawing.Point(328, 100);
            this.RestorePassOkButton.Name = "RestorePassOkButton";
            this.RestorePassOkButton.Size = new System.Drawing.Size(75, 23);
            this.RestorePassOkButton.TabIndex = 0;
            this.RestorePassOkButton.Text = "Send";
            this.RestorePassOkButton.UseVisualStyleBackColor = true;
            this.RestorePassOkButton.Click += new System.EventHandler(this.RestorePassOkButton_Click);
            // 
            // RestorePasscancelButton
            // 
            this.RestorePasscancelButton.Location = new System.Drawing.Point(155, 100);
            this.RestorePasscancelButton.Name = "RestorePasscancelButton";
            this.RestorePasscancelButton.Size = new System.Drawing.Size(75, 23);
            this.RestorePasscancelButton.TabIndex = 0;
            this.RestorePasscancelButton.Text = "Cancel";
            this.RestorePasscancelButton.UseVisualStyleBackColor = true;
            this.RestorePasscancelButton.Click += new System.EventHandler(this.RestorePasscancelButton_Click);
            // 
            // PasswordRestorePanel
            // 
            this.Controls.Add(this.RestorePasswordLabel);
            this.Controls.Add(this.EmailEnterLabel);
            this.Controls.Add(this.EmailEnterTextBox);
            this.Controls.Add(this.RestorePassOkButton);
            this.Controls.Add(this.RestorePasscancelButton);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void RestorePasscancelButton_Click(object sender, System.EventArgs e)
        {
            form.mainPanel.Controls.Clear();
        }

        private void RestorePassOkButton_Click(object sender, System.EventArgs e)
        {
            AutorisationAndRegistrationModel rp = new AutorisationAndRegistrationModel();
            rp.getPasswordToEmail(EmailEnterTextBox.Text);        
        }
    }
}
