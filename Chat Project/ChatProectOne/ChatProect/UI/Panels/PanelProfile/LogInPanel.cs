﻿using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace ChatProect.UI.Panels.PanelProfile
{
    public class LogInPanel : Panel
    {
        private Form1 form;
        public UI.Models.AutorisationAndRegistrationModel ar = new Models.AutorisationAndRegistrationModel();
        public TextBox textPassword;
        public TextBox textLogin;
        public Label labelPassword;
        public Label labelLogin;
        public Button ForgotPasswordButton;
        public Button RegistrationButton;
        private Button LogInButton;

        public LogInPanel(Form1 form)
        {
            this.form = form;
            this.Size = form.panelProfile.Size;
            InitializeComponent();
        }

        public Models.AutorisationAndRegistrationModel AutorisationAndRegistrationModel
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        private void InitializeComponent()
        {
            this.LogInButton = new System.Windows.Forms.Button();
            this.textPassword = new System.Windows.Forms.TextBox();
            this.textLogin = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelLogin = new System.Windows.Forms.Label();
            this.ForgotPasswordButton = new System.Windows.Forms.Button();
            this.RegistrationButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LogInButton
            // 
            this.LogInButton.Location = new System.Drawing.Point(248, 11);
            this.LogInButton.Name = "LogInButton";
            this.LogInButton.Size = new System.Drawing.Size(92, 25);
            this.LogInButton.TabIndex = 0;
            this.LogInButton.Text = "Log In";
            this.LogInButton.UseVisualStyleBackColor = true;
            this.LogInButton.Click += new System.EventHandler(this.LogInButton_Click);
            // 
            // textPassword
            // 
            this.textPassword.Location = new System.Drawing.Point(131, 60);
            this.textPassword.Name = "textPassword";
            this.textPassword.Size = new System.Drawing.Size(78, 20);
            this.textPassword.TabIndex = 7;
            // 
            // textLogin
            // 
            this.textLogin.Location = new System.Drawing.Point(131, 16);
            this.textLogin.Name = "textLogin";
            this.textLogin.Size = new System.Drawing.Size(78, 20);
            this.textLogin.TabIndex = 6;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(53, 67);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(55, 13);
            this.labelPassword.TabIndex = 5;
            this.labelPassword.Text = "password:";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(53, 23);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(32, 13);
            this.labelLogin.TabIndex = 4;
            this.labelLogin.Text = "login:";
            // 
            // ForgotPasswordButton
            // 
            this.ForgotPasswordButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForgotPasswordButton.Location = new System.Drawing.Point(361, 61);
            this.ForgotPasswordButton.Name = "ForgotPasswordButton";
            this.ForgotPasswordButton.Size = new System.Drawing.Size(121, 25);
            this.ForgotPasswordButton.TabIndex = 2;
            this.ForgotPasswordButton.Text = " Forgot password";
            this.ForgotPasswordButton.UseVisualStyleBackColor = true;
            this.ForgotPasswordButton.Click += new System.EventHandler(this.ForgotPasswordButton_Click);
            // 
            // RegistrationButton
            // 
            this.RegistrationButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RegistrationButton.Location = new System.Drawing.Point(361, 11);
            this.RegistrationButton.Name = "RegistrationButton";
            this.RegistrationButton.Size = new System.Drawing.Size(121, 25);
            this.RegistrationButton.TabIndex = 1;
            this.RegistrationButton.Text = "Registration";
            this.RegistrationButton.UseVisualStyleBackColor = true;
            this.RegistrationButton.Click += new System.EventHandler(this.RegistrationButton_Click);
            // 
            // LogInPanel
            // 
            this.Controls.Add(this.labelLogin);
            this.Controls.Add(this.textLogin);
            this.Controls.Add(this.labelPassword);
            this.Controls.Add(this.textPassword);
            this.Controls.Add(this.LogInButton);
            this.Controls.Add(this.RegistrationButton);
            this.Controls.Add(this.ForgotPasswordButton);
           // this.ResumeLayout(false);
          //  this.PerformLayout();

        }

        private void LogInButton_Click(object sender, System.EventArgs e)
        {
            if (textLogin.Text != "" || textPassword.Text != "")
            {
                if (ar.Autorisation(textLogin.Text, textPassword.Text)==true)
                {
                    Autorization();
                    if(ar.isAdmin(textLogin.Text))
                    {
                        form.DeleteRoomButton.Visible = true;
                        form.BanButton.Visible = true;
                    }
                }
                else
                {
                    MessageBox.Show("Логин либо пароль введены неверно!");
                }
            }
            else
            {
                MessageBox.Show("Пара логин и пароль не введена!");
            }

            form.GridViewAllUsers.DataSource = form.allUsers;
        }

        private void Autorization()
        {

            form.panelProfile.Controls.Clear();
            form.panelProfile.Controls.Add(new AfterLogInPanel(form));

            form.roomList = form.roomModel.Read();
            form.GridViewAllRooms.DataSource = form.roomList;

     
        }

        private void RegistrationButton_Click(object sender, System.EventArgs e)
        {
            form.mainPanel.Controls.Clear();
            form.mainPanel.Controls.Add(new MainPanel.RegistrationPanel(form));
        }

        private void ForgotPasswordButton_Click(object sender, System.EventArgs e)
        {
            form.mainPanel.Controls.Clear();
            form.mainPanel.Controls.Add(new MainPanel.PasswordRestorePanel(form));
        }
    }
}
