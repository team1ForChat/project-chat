﻿using System.Drawing;
using System.Windows.Forms;

namespace ChatProect.UI.Panels.PanelProfile
{
    public class AfterLogInPanel : Panel
    {
        private Form1 form;
        private Button EditProfileButton;
        private Button ExitButton;
        private Label UserNameLabel;

        public AfterLogInPanel(Form1 form)
        {
            this.form = form;
            this.Size = form.panelProfile.Size;
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.UserNameLabel = new Label();
            this.EditProfileButton = new Button();
            this.ExitButton = new Button();
            this.SuspendLayout();
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Location = new Point(160, 22);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new Size(137, 13);
            this.UserNameLabel.TabIndex = 0;
            this.UserNameLabel.Text = "You logged in as ";
            // 
            // EditProfileButton
            // 
            this.EditProfileButton.Location = new Point(369, 18);
            this.EditProfileButton.Name = "EditProfileButton";
            this.EditProfileButton.Size = new Size(106, 23);
            this.EditProfileButton.TabIndex = 0;
            this.EditProfileButton.Text = "Edit profile";
            this.EditProfileButton.UseVisualStyleBackColor = true;
            this.EditProfileButton.Click += new System.EventHandler(this.EditProfileButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new Point(369, 61);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new Size(106, 23);
            this.ExitButton.TabIndex = 0;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // AfterLogInPanel
            // 
            this.Controls.Add(this.UserNameLabel);
            this.Controls.Add(this.EditProfileButton);
            this.Controls.Add(this.ExitButton);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void EditProfileButton_Click(object sender, System.EventArgs e)
        {
            form.mainPanel.Controls.Clear();
            form.mainPanel.Controls.Add(new MainPanel.EditProfilePanel(form.mainPanel));
        }

        private void ExitButton_Click(object sender, System.EventArgs e)
        {
            form.panelProfile.Controls.Clear();
            form.panelProfile.Controls.Add(new LogInPanel(form));
        }
    }
}
