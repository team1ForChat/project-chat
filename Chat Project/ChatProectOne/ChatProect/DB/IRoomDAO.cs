﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatProect
{
   public interface IRoomDAO
    {

        void Create(Room r);
        List<Room> Read();
        void Update(Room r);
        void Delete(Room r);
    }
}
