﻿using ChatProect.AutorisationAndRegistration;
using System;
using System.Collections.Generic;


namespace ChatProect.DB
{
    public class MockDAO : IRoomDAO,IUserDAO,IAutorisationAndRegistration
    {
        List<Room> rooms= new List<Room>();
        List<User> users = new List<User>();
        public MockDAO()
        {
            Fill();
        }
        //
        //RoomDAO
        //
        public void Create(Room r)
        {
            rooms.Add(r);
          
        }
        private void Fill()
        {
            User admin = new User("admin", "2", "adasd");
            users.Add(admin);

            Room r1 = new Room("Main Room");
            User u1 = new User("v", "1", "montereydsm@gmail.com");
            r1.userList.Add(u1);
            users.Add(u1);
            rooms.Add(r1);
            Room r2 = new Room("room2");
            User u2 = new User("Vasia", "3", "adasd");
            User u3 = new User("Senya", "3", "adasd");
            r2.userList.Add(u2);
            r2.userList.Add(u3);
            users.Add(u2);
            users.Add(u3);
            rooms.Add(r2);
            Room r3 = new Room("room3");          
            rooms.Add(r3);
            rooms.Add(new Room("room4"));
            rooms.Add(new Room("room5"));
            rooms.Add(new Room("room6"));
            rooms.Add(new Room("room7"));
            rooms.Add(new Room("room8"));
            rooms.Add(new Room("room9"));
            rooms.Add(new Room("room10"));       
        }
       
        public void Delete(Room r)
        {
            rooms.Remove(r);
        }

        public List<Room> Read()
        {
          
            return new List<Room>(rooms);
        }
     

        public void Update(Room r)
        {
            throw new NotImplementedException();
        }
        public List<User> ReadUser()
        {

            return new List<User>(users);
        }
        //
        //UserDAO
        //
        public void Create(User u)
        {
            users.Add(u);
        }

        List<User> IUserDAO.Read()
        {
            return new List<User>(users);
        }

        public void Update(User u)
        {
            throw new NotImplementedException();
        }

        public void Delete(User u)
        {
            users.Remove(u);
        }

        public void getPasswordToEmail(string m)
        {
            foreach (User us in users)
            {
                if (us.eMail == m)
                {
                    AutorisationAndRegistration.RestorePassword rp =new AutorisationAndRegistration.RestorePassword(m,us.password);
                   
                }                                                
            }
          
        }

        public bool Autorisation(string login, string password)
        {
            Autorisation a = new Autorisation();
            return a.Autorization(login, password);
        }

        public bool isAdmin(string login)
        {
            Autorisation a = new Autorisation();
            return a.isAdmin(login);
        }
    }
}
