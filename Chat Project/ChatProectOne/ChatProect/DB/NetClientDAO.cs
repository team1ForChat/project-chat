﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ChatProect.DB
{
    class NetClientDAO : IRoomDAO, IUserDAO,IAutorisationAndRegistration
    {
        static TcpClient client;
        static NetworkStream stream;
        static List<Room> rooms = new List<Room>();
        static List<User> users = new List<User>();
        public NetClientDAO()
        {
            client = new TcpClient();
            client.Connect("127.0.0.1", 8888);
            stream = client.GetStream();
            Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
            receiveThread.Start();
        }

        public UI.Models.AutorisationAndRegistrationModel AutorisationAndRegistrationModel
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public RoomModel RoomModel
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        public UserModel UserModel
        {
            get
            {
                throw new System.NotImplementedException();
            }

            set
            {
            }
        }

        static void SendMessage(string message)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }
        static void ReceiveMessage()
        {
            while (true)
            {
                byte[] data = new byte[64];
                StringBuilder builder = new StringBuilder();
                int bytes = 0;
                do
                {
                    bytes = stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (stream.DataAvailable);

                string message = builder.ToString();
                users = DecodeMessage(message);
            }
        }   
     
        static List<User> DecodeMessage(string message)
        {
            List<User> lst = new List<User>();
            string[] stringsOfUsers = message.Split(';');
            for (int i = 1; i < stringsOfUsers.Length; i++)
            {
                if (!string.IsNullOrEmpty(stringsOfUsers[i]))
                {
                    string[] stringP = stringsOfUsers[i].Split(',');
                    User u = new User(stringP[0], stringP[1], stringP[2]);                                   
                    lst.Add(u);
                }
            }
            return lst;
        }
        public void Create(Room r)
        {
            string s = "CREATEROOM:";
            s += r.name;
            SendMessage(s);
        }

        public void Create(User u)
        {
            string s = "CREATEUSER:";
            s += u.login + "," + u.password + "," + u.eMail;
            SendMessage(s);
        }

        public void Delete(Room r)
        {
            string s = "DELETEROOM:";
            s += r.name;
            SendMessage(s);
        }

        public void Delete(User u)
        {
            string s = "DELETEUSER:";
            s += u.login + "," + u.password + "," + u.eMail;
            SendMessage(s);
        }

        public List<User> Read()
        {
            SendMessage("READUSERS:");
            Thread.Sleep(1000);
            return new List<User>(users);
        }

        public void Update(Room r)
        {
            string s = "UPDATEROOM:";
            s += r.name;
            SendMessage(s);
        }

        public void Update(User u)
        {
            string s = "UPDATEUSER:";
            s += u.login + "," + u.password + "," + u.eMail;
            SendMessage(s);
        }

        List<Room> IRoomDAO.Read()
        {
            SendMessage("READROOMS:");
            Thread.Sleep(1000);
            return new List<Room>(rooms);
        }

        public void getPasswordToEmail(string m)
        {
            throw new NotImplementedException();
        }

        public bool Autorisation(string login, string password)
        {
            throw new NotImplementedException();
        }

        public bool isAdmin(string login)
        {
            throw new NotImplementedException();
        }
    }
}
