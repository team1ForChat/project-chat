﻿namespace ChatProect.DB
{
    interface IAutorisationAndRegistration
    {
        void getPasswordToEmail(string m);

        bool Autorisation(string login, string password);

        bool isAdmin(string login);
    }
}
