﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatProect.DB
{
    interface IUserDAO
    {
        void Create(User u);
        List<User> Read();
        void Update(User u);
        void Delete(User u);

    }
}
